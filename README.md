Setup local hosting via docker:
--
    docker-compose up


To install new python packages, run :

    docker-compose exec web pip install <packageName> && pip freeze > requirements.txt

To manage migrations when database model is updated, run :

    docker-compose exec web python run.py db migrate

To feed database with sample datas, run :

    docker-compose exec web python run.py feed

Env vars to set up and commands to run if you host the app elsewhere than docker:

    DATABASE_URL = provider://username:password@server/db
    APP_SETTINGS = config.[Dev/Prod]Config
    run python run.py db upgrade
    run python run.py runserver



Setup local hosting via nginx
--
 1/ `touch bibtracker_api.ini`

    [uwsgi]
    module = app:app

    master = true
    processes = 5

    socket = bibtracker_api.sock
    chmod-socket = 660
    vacuum = true

    die-on-term = true

    env=DATABASE_URL=xx
    env=APP_SETTINGS=config.xxConfig


2/ `touch bibtracker_api.service`

    [Unit]
    Description=uWSGI instance to serve bibtracker_api
    After=network.target

    [Service]
    User=www-data
    Group=www-data
    WorkingDirectory=/pathtoproject/bibtracker_api
    Environment="PATH=/pathtoproject/bibtracker_api/venv/bin"
    ExecStart=/pathtoproject/bibtracker_api/venv/bin/uwsgi --ini bibtracker_api.ini

    [Install]
    WantedBy=multi-user.target


Needed dependencies:

python-mysqldb python3-dev libmysqlclient-dev or libmariadbclient-dev