FROM python:2.7.13
LABEL MAINTAINER="Guillaume DEPOUILLY"
ENV APP_SETTINGS=config.DevConfig
ENV DATABASE_URL=postgresql://postgres:postgres@db/postgres
EXPOSE 5000
WORKDIR /usr/src/app
RUN git clone https://github.com/vishnubob/wait-for-it
COPY requirements.txt /usr/src/app/
RUN echo "cd /usr/src/app/web && \
    python run.py db upgrade && \
    python run.py runserver --host='0.0.0.0'" > startup-script.sh
RUN chmod +x startup-script.sh
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt
RUN mkdir /usr/src/app/web
WORKDIR /usr/src/app/web