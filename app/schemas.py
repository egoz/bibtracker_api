from app import ma
from app.models import Bottle, User


class BottleSchema(ma.ModelSchema):
    observations = None
    class Meta:
        model = Bottle

class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
