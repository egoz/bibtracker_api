from flask import render_template, jsonify, request, abort
from flask_httpauth import HTTPBasicAuth
from app import app, db
from app.models import Bottle, User
from app.schemas import BottleSchema, UserSchema
from sqlalchemy.exc import IntegrityError
import datetime
import time


auth = HTTPBasicAuth()
bottle_schema = BottleSchema()
bottles_schema = BottleSchema(many=True)
user_schema = UserSchema(many=True)


@auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username = username).first()
    if not user or not user.verify_password(password):
        return False
    return True


@app.route('/auth')
@auth.login_required
def check_login():
    return jsonify({'success': 'true'});


@app.route('/')
def index():
    """Render the homepage"""
    return render_template('index.html',
                           title='bibtracker')


@app.route('/user/add', methods = ['POST'])
def create_user():
    """Create a new user"""
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        return jsonify({"message": "Malformed request"}), 400
    if User.query.filter_by(username = username).first() is not None:
        return jsonify({"message": "Username already exists"}), 400
    user = User(username = username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify({ 'username': user.username }), 201


@app.route('/user/get')
def get_user():
    users = User.query.all()
    result = user_schema.dump(users)
    return jsonify({'users': result})


@app.route('/bottle/get')
@auth.login_required
def get_bottles():
    """Returns all bottles"""
    bottles = Bottle.query.all()
    result = bottles_schema.dump(bottles)
    return jsonify({'bottles': result.data})


@app.route('/bottle/get/<int:id>')
def get_bottle(id):
    """Returns a specific bottle"""
    bottle = Bottle.query.filter(Bottle.id == id).one_or_none()
    if bottle is None:
        return jsonify({"message": "Bottle does not exists"}), 400
    result = bottle_schema.dump(bottle)
    return jsonify({'bottle': result.data})


@app.route('/bottle/add', methods=['POST'])
def add_bottle():
    """Add a new bottle"""
    if not request.json or not 'quantity' in request.json:
        return jsonify({"message": "Malformed request"}), 400
    schema = BottleSchema()
    bottle = Bottle(quantity=request.json['quantity'],
                    observations=request.json['observations'],
                    datetime=datetime.datetime.utcnow(),
                    timestmp=time.time())
    db.session.add(bottle)
    db.session.commit()
    result = schema.dump(bottle)
    return jsonify(result), 201


@app.route('/bottle/delete/<int:id>', methods=['DELETE'])
def delete_bottle(id):
    """Delete a specific bottle"""
    bottle = Bottle.query.filter(Bottle.id == id).one_or_none()
    if bottle is None:
        return jsonify({"message": "Bottle does not exists"}), 400
    db.session.delete(bottle)
    db.session.commit()
    return jsonify({'status': 'deleted', 'id': id})