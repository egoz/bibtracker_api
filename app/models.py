from app import db
from passlib.apps import custom_app_context


class Bottle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer, nullable=False)
    datetime = db.Column(db.DateTime, nullable=False)
    timestmp = db.Column(db.Integer, nullable=False)
    observations = db.Column(db.String(50))

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index = True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        self.password_hash = custom_app_context.hash(password)

    def verify_password(self, password):
        return custom_app_context.verify(password, self.password_hash)