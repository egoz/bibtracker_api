#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from flask import Flask
from flask_cors import CORS, cross_origin
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
CORS(app)
app.config.from_object('config.Config')
app.config.from_object(os.environ['APP_SETTINGS'])

db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

from app import views


@manager.command
def seed():
    """Feed database with samples datas"""
    from .models import User, Bottle
    import datetime
    import time
    if User.query.filter_by(username = 'user').first() is None:
        u = User(
            username='user', 
            password_hash='$6$rounds=656000$Hgs0vDoC62izR6ro$mwQf75Id9HJEr2oSwsS0of6kHPhx833CrwctIoTS.Qq1/X5B9MGezuxKyMg.ucoBg.7.6tpCj4NwmzoWrZqTW.'
        )
        # password is 'password' without quotes
        db.session.add(u)
    b1 = Bottle(
        quantity=10,
        observations='',
        datetime=datetime.datetime.utcnow(),
        timestmp=time.time()
    )
    b2 = Bottle(
        quantity=250,
        observations='big one !',
        datetime=datetime.datetime.utcnow(),
        timestmp=time.time()
    )
    b3 = Bottle(
        quantity=120,
        observations='another one',
        datetime=datetime.datetime.utcnow(),
        timestmp=time.time()
    )
    for b in range(0, 2):
        db.session.add('b' + b)
    db.session.commit()