from app import app, db
from app.models import Bottle
import unittest
import os
basedir = os.path.abspath(os.path.dirname(__file__))
import time
import datetime


class BibtrackerTest(unittest.TestCase): 

    @classmethod
    def setUpClass(cls):
        pass 

    @classmethod
    def tearDownClass(cls):
        pass 

    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        app.config['TESTING'] = True
        app.config['DATABASE_URL'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_home_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.app.get('/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200) 

    def test_home_data(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.app.get('/')

        # assert the response data
        self.assertIn('Hello World', result.data)

    def test_bottle(self):
        b = Bottle(
            quantity='1', 
            datetime=datetime.datetime.utcnow(), 
            timestmp=time.time(), 
            observations='test'
        )
        self.assertEqual(b.observations, 'test')

if __name__ == '__main__':
    unittest.main()